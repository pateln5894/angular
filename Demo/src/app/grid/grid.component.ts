import { Component, ViewChild, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AgGridAngular } from 'ag-grid-angular';


@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss']
})
export class GridComponent implements OnInit {

  //@ViewChild('agGrid') agGrid: AgGridAngular;

  private gridApi: any;
  private columnApi: any;
  params :any;

  columnDefs = [
      {headerName: 'Make', field: 'make', sortable: true, filter: true, checkboxSelection: true },
      {headerName: 'Model', field: 'model', sortable: true, filter: true },
      {headerName: 'Price', field: 'price', sortable: true, filter: true }
  ];

  rowData: any;

  constructor(private http: HttpClient) {

  }

  ngOnInit() {
this.http.get('https://api.myjson.com/bins/15psn9').subscribe(data =>
{
  this.params.api.setRowData(data);
})
     // this.rowData = this.http.get('https://api.myjson.com/bins/15psn9');
  }

   onGridReady(params){
    this.gridApi = params.api;
    this.columnApi = params.columnApi;
   }

  getSelectedRows() {
    console.log("test");
    //   const selectedNodes = this.gridApi.api.getSelectedNodes();
    //   const selectedData = selectedNodes.map( node => node.data );
    //   const selectedDataStringPresentation = selectedData.map( node => node.make + ' ' + node.model).join(', ');
    //  console.log(selectedDataStringPresentation);
      // alert(`Selected nodes: ${selectedDataStringPresentation}`);
  }
}