import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExceptionComponent } from './exception/exception.component';
import { SearchComponent } from './search/search.component';
import { DetailsComponent } from './details/details.component';
import { GridComponent } from './grid/grid.component';

const routes: Routes = [
  {path:'', redirectTo:'search', pathMatch:'full' },
  { path: 'search', component: SearchComponent },
  { path: 'grid', component: GridComponent },
  { path: 'details', component: DetailsComponent },
  { path: 'exception', component: ExceptionComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponent = [ExceptionComponent, SearchComponent, GridComponent, DetailsComponent]
