import { Component, OnInit, NgModule} from '@angular/core';
import { ReactiveFormsModule, FormGroup, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { SearchService } from '../search.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  isOrderPlacer : boolean = true;
  isExpId : boolean = false;
  example:any;
  rowData:any;
  //form = new FormGroup();
  //  form1 = new FormGroup({
  //   make: new FormControl(''),
  //   model: new FormControl(''),
  //   price: new FormControl('')
  // });
  
  //console.log(form.value);   // {first: null, last: null}
  
  

  form1: FormGroup;
  //example = { make: "", model: "", price:0};

  constructor(private searchService : SearchService) {
   // this.rowData =  http.get('https://api.myjson.com/bins/15psn9');
    
    //builder: FormBuilder
    // this.form1 = builder.group({
    //   make: "",
    //   model: "",
    //   price:0
    // });
  }

  columnDefs = [
    {headerName: 'Make', field: 'make', rowGroup: true  },
    {headerName: 'Model', field: 'model' },
    {headerName: 'Price', field: 'price'}
];


  ngOnInit() {
this.searchService.getData().subscribe(data => this.rowData =data);
console.log(this.rowData);

  }

  orderPlacer(){
    this.isOrderPlacer = true;
    this.isExpId = false;
  }

  expId()
    {
      this.isOrderPlacer = false;
      this.isExpId = true;
    }

  Search()
{
  console.log("Search click");
  
//   this.rowData = [
//     { make: 'Toyota', model: 'Celica', price: 35000 },
//     { make: 'Ford', model: 'Mondeo', price: 32000 },
//     { make: 'Ford', model: 'Mondeo1', price: 320000 },
//     { make: 'Porsche', model: 'Boxter', price: 72000 }
// ];
}

Clear(){
console.log("clear clicked");
  this.rowData = [];
}

  onCellClicked(data1){
    console.log("row clicked");
    this.example = data1.data;
  console.log(data1);
 // this.form1.setValue({make: 'Nancy', model:'model', price: 1000});
  //console.log(this.form1.value);
  }

  onCellDoubleClicked(data2){
    console.log(data2)
  }
}
