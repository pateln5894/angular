export interface IDocumentCategory{
    make: string,
    model: string,
    price: number
}