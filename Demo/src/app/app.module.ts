import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule, routingComponent } from './app-routing.module';
import { AppComponent } from './app.component';
import { AgGridModule } from 'ag-grid-angular';
import 'ag-grid-enterprise';
import { GridComponent } from './grid/grid.component';
import { SearchComponent } from './search/search.component';
import {  ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SearchService } from './search.service';


@NgModule({
  declarations: [
    AppComponent, 
    routingComponent
   // SearchService
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
   // ReactiveFormsModule,
    AgGridModule.withComponents([GridComponent,SearchComponent]),
    HttpClientModule
   
  ],
  providers: [SearchService],
  bootstrap: [AppComponent]
})
export class AppModule { }
