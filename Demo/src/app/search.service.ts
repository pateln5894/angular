import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IDocumentCategory } from './models/search';

@Injectable()
export class SearchService {

  private uri :string ="/assets/data/search.json";
  constructor(private http:HttpClient) { }

  getData(): Observable<IDocumentCategory[]>{
   return this.http.get<IDocumentCategory[]>(this.uri);
 }

}
